﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MetaMedio : MonoBehaviour
{
    public Text tiempo;
    public Text tiempoTotal;
    public Text tiempoRecord;
    public AudioSource meta;
    public GameObject menu;

    int record;
    float tiempoActual = 0f;
    bool fin = false;

    // Start is called before the first frame update
    void Start()
    {
        record = PlayerPrefs.GetInt("recordMedio"); //Para guardar datos utilizamos el PlayerPrefs
        tiempo.text = "Tiempo: " + tiempoActual;
    }

    // Update is called once per frame
    void Update()
    {
        if (fin == false)
        {
            tiempoActual = tiempoActual + (1 * Time.deltaTime);
            tiempo.text = "Tiempo: " + tiempoActual.ToString("f0") + " '";
        }
    }

    public void FinDejuego()
    {
        fin = true; //Para que no se efectue el update
        meta.Play(); //Sonido
        menu.SetActive(true); //Menú
        tiempoTotal.text = "" + tiempoActual.ToString("f0") + " segundos";

        if (record > tiempoActual)
        {
            record = (int)tiempoActual;
            PlayerPrefs.SetInt("recordMedio", record);
        }
        tiempoRecord.text = "" + record + " segundos";
    }
}