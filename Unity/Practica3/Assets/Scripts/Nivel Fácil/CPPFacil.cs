﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CPPFacil : MonoBehaviour
{
    public Camera FPSCamera;

    public float horizontalSpeed;
    public float verticalSpeed;

    float h; // Guardo movimiento del raton horizontalmente
    float v; // Guardo movimiento del raton verticalmente

    public MetaFacil metaFacil;

    bool fin = false;
    bool mov = false;

    public AudioSource andar;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (fin == false)
        {
            h = horizontalSpeed * Input.GetAxis("Mouse X");
            v = verticalSpeed * Input.GetAxis("Mouse Y");

            transform.Rotate(0, h, 0);
            FPSCamera.transform.Rotate(-v, 0, 0);

            if (Input.GetKey(KeyCode.LeftArrow))
            {
                transform.Translate(0, 0, 0.2f);
                mov = true;
            }
            else
            {
                if (Input.GetKey(KeyCode.RightArrow))
                {
                    transform.Translate(0, 0, -0.2f);
                    mov = true;
                }
                else
                {
                    if (Input.GetKey(KeyCode.DownArrow))
                    {
                        transform.Translate(-0.2f, 0, 0);
                        mov = true;
                    }
                    else
                    {
                        if (Input.GetKey(KeyCode.UpArrow))
                        {
                            transform.Translate(0.2f, 0, 0);
                            mov = true;
                        }
                    }
                }
            }
            if (mov == true)
            {
                if (!(Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow)))
                {
                    andar.Play();
                }
            }
        }
    }

    void OnCollisionEnter(Collision objeto) // Metodo para recoger la colision de la bola
    {
        if (objeto.collider.tag == "meta")
        {
            andar.Stop();
            fin = true;
            metaFacil.FinDejuego();
        }
        if (objeto.collider.tag == "noTocar")
        {
            transform.position = new Vector3(-24.23f, 2.67f, -16.6f);
        }
    }
}
