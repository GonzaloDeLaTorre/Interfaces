﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TileManager : MonoBehaviour
{
    public GameObject Baldosa;
    public GameObject nuevaBaldosa;
    private static TileManager instancia;

    int i = 0;
    int cnt = 0;

    public static TileManager Instancia
    {
        get
        {
            if (instancia == null)
            {
                instancia = GameObject.FindObjectOfType<TileManager>();
            }
            return TileManager.instancia;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        while (cnt < 10) //Baldosas iniciales
        {
            CrearBaldosa();
            cnt++;
        }
    }

    public void CrearBaldosa()
    {
        int opcion = Random.Range(0, 2);
        // Si es 0 crea la baldosa a la izquierda.
        if (opcion == 0)
        {
            Baldosa = (GameObject)Instantiate(nuevaBaldosa, new Vector3(nuevaBaldosa.transform.position.x - 1, nuevaBaldosa.transform.position.y, nuevaBaldosa.transform.position.z), Quaternion.identity);
            if (i == 1 || i == 7)
            {
                nuevaBaldosa.transform.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                nuevaBaldosa.transform.GetChild(1).gameObject.SetActive(false);
            }
            // Guarda como ultimaBaldosa a la que acabamos de crear.
            nuevaBaldosa = Baldosa;
        }
        else
        {
            Baldosa = (GameObject)Instantiate(nuevaBaldosa, new Vector3(nuevaBaldosa.transform.position.x, nuevaBaldosa.transform.position.y, nuevaBaldosa.transform.position.z + 1), Quaternion.identity);
            if (i == 3 || i == 10)
            {
                nuevaBaldosa.transform.GetChild(1).gameObject.SetActive(true);
            }
            else
            {
                nuevaBaldosa.transform.GetChild(1).gameObject.SetActive(false);
            }
            // Guarda como ultimaBaldosa a la que acabamos de crear.
            nuevaBaldosa = Baldosa;
        }
        i++;
        if (i == 10)
        {
            i = 0;
        }
    }

    public IEnumerator DestruirBaldosa(GameObject baldosa, Rigidbody rigibody)
    {
        // La baldosa se cae.
        yield return new WaitForSeconds((float)0.5);
        rigibody.isKinematic = false;
        // La baldosa se destruye.
        yield return new WaitForSeconds((float)0.7);
        Destroy(baldosa);
    }

}
