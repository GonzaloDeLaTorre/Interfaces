﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CodigoMoneda : MonoBehaviour
{

    public ParticleSystem particulas;
    public AudioSource sonidoPuntoDiamante;
    public PointManager pointManager;

    public float velocidadMoneda = 30f;
        

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            pointManager.SumarDiamante();
            Instantiate(particulas, transform.position, Quaternion.identity).Play();
            gameObject.SetActive(false);
            sonidoPuntoDiamante.Play();
        }
    }

    private void Update()
    {
        // Movimiento de la moneda
        transform.Rotate(Vector3.up * velocidadMoneda * Time.deltaTime, Space.World);
    }

}
