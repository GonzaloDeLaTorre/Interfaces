﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoBola : MonoBehaviour
{
    public float speed;
    private Vector3 dir;
    private bool direction;
    public PointManager pointManager;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            if (direction)
            {
                dir = Vector3.left;
                direction = false;
            }
            else
            {
                dir = Vector3.forward;
                direction = true;
            }   
        }

        transform.Translate(dir * Time.deltaTime * speed);

        if (transform.position.y < 0.5)
        {
            FinDeJuego();
            
        }
    }

    private void FinDeJuego()
    {
        pointManager.FinDejuego();
    }

}
