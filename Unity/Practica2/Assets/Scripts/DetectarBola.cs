﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DetectarBola : MonoBehaviour
{
    public PointManager pointManager;
    public GameObject baldosa;
    public Rigidbody rigibody;

    void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            pointManager.SumarBaldosa();
            TileManager.Instancia.CrearBaldosa();
            StartCoroutine(TileManager.Instancia.DestruirBaldosa(baldosa, rigibody));
        }
    }
}
