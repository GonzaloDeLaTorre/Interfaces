﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PointManager : MonoBehaviour
{
    public Text score;
    public Text puntuacion;
    public Text puntuacionRecord;
    public AudioSource gameOver;
    public GameObject menu;

    int record;
    int puntos = 0;

    // Start is called before the first frame update
    void Start()
    {
        record = PlayerPrefs.GetInt("record"); //Para guardar datos utilizamos el PlayerPrefs
        score.text = "Puntos: " + puntos;
    }

    public void FinDejuego()
    {
        gameOver.Play(); //Sonido
        menu.SetActive(true); //Menú
        puntuacion.text = "" + puntos;
       
        if (record < puntos)
        {
            record = puntos;
            PlayerPrefs.SetInt("record", record);
        }
        puntuacionRecord.text = "" + record;
    }

    public void SumarBaldosa()
    {
        puntos = puntos + 1;
        score.text = "Puntos: " + puntos;
    }

    public void SumarDiamante()
    {
        puntos = puntos + 10;
        score.text = "Puntos: " + puntos;
    }
}
