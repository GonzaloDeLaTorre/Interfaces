﻿

using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{
    public GameObject ball;
    private Vector3 posicionRelativa;
    private Vector3 posicionCaida;

    // Use this for initialization
    void Start()
    {
        posicionRelativa = transform.position - ball.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        posicionCaida = transform.position;
        transform.position = ball.transform.position + posicionRelativa;

        if (ball.transform.position.y < 0)
        {
            transform.position = posicionCaida;
        }
    }
}

