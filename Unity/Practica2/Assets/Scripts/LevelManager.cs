﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Linq;

public class LevelManager : MonoBehaviour
{
    // ToggleGroup toggleGroupInstance;
    public Toggle facil;
    public Toggle medio;
    public Toggle dificil;


    // Start is called before the first frame update
    void Start()
    {
      //  toggleGroupInstance = GetComponent<ToggleGroup> ();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CargaNivel(string nombreNivel)
    {
        if (facil.isOn)
        {
            SceneManager.LoadScene("JugarFacil");
        }
        if (medio.isOn)
        {
            SceneManager.LoadScene("JugarMedio");
        }
        if (dificil.isOn)
        {
            SceneManager.LoadScene("JugarDificil");
        }
        //SceneManager.LoadScene(nombreNivel);
    }

    public void CargaNivel2(string nombreNivel)
    {
        SceneManager.LoadScene(nombreNivel);
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
