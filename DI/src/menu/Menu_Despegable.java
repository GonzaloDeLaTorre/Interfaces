/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package menu;

import javax.swing.JOptionPane;

/**
 *
 * @author alumno
 */
public class Menu_Despegable extends javax.swing.JFrame {

	/**
	 * Creates new form Menu_Despegable1
	 */
	public Menu_Despegable() {
		initComponents();
		setLocationRelativeTo(null);
	}

	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu_Archivo = new javax.swing.JMenu();
        jMenu_Enviar = new javax.swing.JMenu();
        jMenuItem_Impresora = new javax.swing.JMenuItem();
        jMenuItem_Email = new javax.swing.JMenuItem();
        jMenuItem_Salir = new javax.swing.JMenuItem();
        jMenu_AcercaDe = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jMenu_Archivo.setMnemonic('a');
        jMenu_Archivo.setText("Archivo");

        jMenu_Enviar.setMnemonic('e');
        jMenu_Enviar.setText("Enviar");

        jMenuItem_Impresora.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_I, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Impresora.setMnemonic('i');
        jMenuItem_Impresora.setText("Impresora");
        jMenuItem_Impresora.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_ImpresoraActionPerformed(evt);
            }
        });
        jMenu_Enviar.add(jMenuItem_Impresora);

        jMenuItem_Email.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Email.setMnemonic('e');
        jMenuItem_Email.setText("Email");
        jMenuItem_Email.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_EmailActionPerformed(evt);
            }
        });
        jMenu_Enviar.add(jMenuItem_Email);

        jMenu_Archivo.add(jMenu_Enviar);

        jMenuItem_Salir.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem_Salir.setMnemonic('s');
        jMenuItem_Salir.setText("Salir");
        jMenuItem_Salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem_SalirActionPerformed(evt);
            }
        });
        jMenu_Archivo.add(jMenuItem_Salir);

        jMenuBar1.add(jMenu_Archivo);

        jMenu_AcercaDe.setMnemonic('c');
        jMenu_AcercaDe.setText("Acerca de");
        jMenu_AcercaDe.addMenuListener(new javax.swing.event.MenuListener() {
            public void menuSelected(javax.swing.event.MenuEvent evt) {
                jMenu_AcercaDeMenuSelected(evt);
            }
            public void menuDeselected(javax.swing.event.MenuEvent evt) {
            }
            public void menuCanceled(javax.swing.event.MenuEvent evt) {
            }
        });
        jMenuBar1.add(jMenu_AcercaDe);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 279, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuItem_SalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_SalirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_jMenuItem_SalirActionPerformed

    private void jMenuItem_ImpresoraActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_ImpresoraActionPerformed
        JOptionPane.showMessageDialog(this, "Has pulsado 'Impresora'.", "Impresora", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem_ImpresoraActionPerformed

    private void jMenuItem_EmailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem_EmailActionPerformed
        JOptionPane.showMessageDialog(this, "Has pulsado 'Email'.", "Email", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenuItem_EmailActionPerformed

    private void jMenu_AcercaDeMenuSelected(javax.swing.event.MenuEvent evt) {//GEN-FIRST:event_jMenu_AcercaDeMenuSelected
        JOptionPane.showMessageDialog(this, "Has pulsado 'Acerca de'.", "Acerca de", JOptionPane.INFORMATION_MESSAGE);
    }//GEN-LAST:event_jMenu_AcercaDeMenuSelected

	/**
	 * @param args the command line arguments
	 */
	public static void main(String args[]) {
		/* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
		 * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
		 */
		try {
			for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					javax.swing.UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (ClassNotFoundException ex) {
			java.util.logging.Logger.getLogger(Menu_Despegable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (InstantiationException ex) {
			java.util.logging.Logger.getLogger(Menu_Despegable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			java.util.logging.Logger.getLogger(Menu_Despegable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		} catch (javax.swing.UnsupportedLookAndFeelException ex) {
			java.util.logging.Logger.getLogger(Menu_Despegable.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
		}
        //</editor-fold>
        //</editor-fold>

		/* Create and display the form */
		java.awt.EventQueue.invokeLater(new Runnable() {
			public void run() {
				new Menu_Despegable().setVisible(true);
			}
		});
	}

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem_Email;
    private javax.swing.JMenuItem jMenuItem_Impresora;
    private javax.swing.JMenuItem jMenuItem_Salir;
    private javax.swing.JMenu jMenu_AcercaDe;
    private javax.swing.JMenu jMenu_Archivo;
    private javax.swing.JMenu jMenu_Enviar;
    // End of variables declaration//GEN-END:variables
}
