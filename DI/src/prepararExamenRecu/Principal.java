/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prepararExamenRecu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import practicaEvaluable1.Ventas;

/*
    TENGO UNA BBDD DE CLIENTES, LO QUIERO PASAR A UN FICHERO DE VENTAS REALIZADAS.
*/

/**
 *
 * @author Gonzalo
 */
public class Principal {
//     java.sql.Connection con = null;
//    String nombre="";
//    String apellidos="";
//    int edad=0;
    
    public static void main(String[] args) throws Exception {
        //Conectamos con BD
        GestorBD g = new GestorBD();
        java.sql.Connection con = g.conectarBBDD();
        
        //Creamos ArrayList de Cliente
        ArrayList<Cliente> aln = new ArrayList<Cliente>();
        
        //Leemos la informacion de la bbdd y lo guardamos en el ArrayList de Cliente
        aln = leerClientesBBDD(con);
        
//        Comprobar que guarda bien en el ArrayList
//        for (int i = 0; i < aln.size(); i++) {
//            System.out.println(aln.get(i));
//        }
        
        //A partir del ArrayList de Cliente ya con la informacion de la bbdd, lo guardamos en un fichero(PrintWriter)
        grabarClientesFichero(aln);
        
        //Pasamos los datos del fichero Cliente.txt a un nuevo fichero.
        escribirNuevoFichero();
        
        //Pasar info del fichero a la bbdd
        updateTabla(con, aln);
    }

    private static ArrayList<Cliente> leerClientesBBDD(Connection con) throws Exception {
        java.sql.Connection conexion = con;	
        
        String buscar = "SELECT * FROM tabla_clientes;";
        Statement st = conexion.createStatement();
        ResultSet rs = st.executeQuery(buscar);
        
        ArrayList<Cliente> al = new ArrayList<Cliente>();
        while (rs.next()) {
            al.add(new Cliente(rs.getString(1)+";", rs.getString(2)+";", Integer.valueOf(rs.getString(3))));
        }
        rs.close();
        st.close();
        
        return al;
    }

    private static void grabarClientesFichero( ArrayList<Cliente> aln) throws IOException {
        
        PrintWriter pw = new PrintWriter(new FileWriter(new File("Cliente.txt"), true)); // true deja los antiguos y añade los nuevos, false elimina los antiguos y añade los nuevos
        for (int i = 0; i < aln.size(); i++) {
            /*Da igual la primera forma que la segunda*/
            pw.print(aln.get(i).getNombre());
            pw.print(aln.get(i).getApellido());
            pw.println(aln.get(i).getEdad()+";");
//            pw.println(aln.get(i));
        }
        pw.close();
    }
    

    private static void escribirNuevoFichero() throws FileNotFoundException, IOException {
        ArrayList<Cliente> aln = new ArrayList<Cliente>();
        aln = leerFichero();
        
        PrintWriter pw = new PrintWriter(new FileWriter(new File("Cliente222.txt"), true)); // true deja los antiguos y añade los nuevos, false elimina los antiguos y añade los nuevos
        for (int i = 0; i < aln.size(); i++) {
            pw.print(aln.get(i).getNombre()+";");
            pw.print(aln.get(i).getApellido()+";");
            pw.println(aln.get(i).getEdad()+";");
//            pw.println(al.get(i).toString());
        }
        pw.close();
    }
    
    private static ArrayList<Cliente> leerFichero() throws FileNotFoundException, IOException {
        ArrayList<Cliente> aln = new ArrayList<Cliente>();
        
        BufferedReader br = new BufferedReader(new FileReader(new File("Cliente.txt")));
        String linea;
        while ((linea = br.readLine()) != null) {
            String[] atributos = linea.split(";");

            String nombreCliente = atributos[0];
            String apellidosCliente = atributos[1];
            int edad = Integer.valueOf(atributos[2]);

            aln.add(new Cliente(nombreCliente, apellidosCliente, edad));
        }
        br.close();
        
        return aln;
    }


    private static void updateTabla(Connection con, ArrayList<Cliente> al) throws SQLException {
        /*
            Me salta error porque la primary key(columna 1 en este caso) no puede duplicarse, ya existe el mismo Nombre.
        */
        java.sql.Connection conexion = con;
        
        java.sql.PreparedStatement pst = null;
        for (int i = 0; i < al.size(); i++) {
            String[] atributos = al.get(i).toString().split(";");

            String nombreCliente = atributos[0];
            String apellidosCliente = atributos[1];
            int edad = Integer.valueOf(atributos[2]);
            
            String insert="INSERT INTO tabla_clientes (nombre,apellidos,edad) VALUES ('"+nombreCliente+"','"+apellidosCliente+"',"+edad+")";		
            pst = conexion.prepareStatement(insert);
            pst.executeUpdate();
        }
        
        pst.close();
    }

}
