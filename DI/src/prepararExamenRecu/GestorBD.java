/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package prepararExamenRecu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Gonzalo
 */
public class GestorBD {
    
    static java.sql.Connection con = null;

    public static Connection conectarBBDD() throws SQLException {
        try { 
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            System.err.println("No se encontró el Drive MySQL para JDBC.");
        }
        con = DriverManager.getConnection("jdbc:mysql://localhost/clientes", "root", "");
        return con;
    }
}
