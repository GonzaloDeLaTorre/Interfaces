package prepararExamen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JOptionPane;
import javax.xml.transform.Result;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class InterfazUsuarioConsola {

	public static void main(String[] args) throws Exception {

		java.sql.Connection con=conectarBBDD();

		listadoNormal(con);
		
		listadoCP(con);
		
		listadoEmail(con);
		
		listadoNombre(con);
		
		listadoPais(con);
		
//		darAltaPersona(con);
		
		borrarPersona(con);

	}



	private static void borrarPersona(java.sql.Connection con) throws Exception {
		java.sql.Connection conexion = con;
		String selec="DELETE FROM persona WHERE nombre='Gonzalo'";
		java.sql.PreparedStatement pst = conexion.prepareStatement(selec);
		pst.executeUpdate();
		
		pst.close();
	}



	private static void darAltaPersona(java.sql.Connection con) throws Exception { //MAAAAAL
		java.sql.Connection conexion = con;		
		
		String comprobar="SELECT * FROM persona";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(comprobar);
		
		ArrayList<String>al=new ArrayList<String>();
		while(rs.next()) {
			al.add(rs.getString(1)+"   "+rs.getString(2)+"   "+rs.getString(3)+"   "+rs.getString(4));
		}
		
//		while(rs.next()) {
//			if (rs.getString(1).equals("Gonzalo")) {
//				java.sql.PreparedStatement pst;
//				String update="UPDATE persona SET nombre = 'Gonzalo' WHERE nombre = 'Gonzalo'";		
//				pst = conexion.prepareStatement(update);
//				pst.executeUpdate();
//				pst.close();
//			} else {
//				java.sql.PreparedStatement pst;
//				String insert="INSERT INTO persona (nombre,CP,pais,email) VALUES (\"Gonzalo\",\"28430\",\"España\",\"g.dlt@gmail.com\")";		
//				pst = conexion.prepareStatement(insert);
//				pst.executeUpdate();
//				pst.close();
//			}
//		}
		
//		for (String i : al) {
//			System.out.println(i);
//		}
		
		for (int i = 0; i < al.size(); i++) {
			if (al.get(i).toString().equals("Gonzalo")) {
				java.sql.PreparedStatement pst;
				String update="UPDATE persona SET nombre = 'Gonzalo' WHERE nombre = 'Gonzalo'";		
				pst = conexion.prepareStatement(update);
				pst.executeUpdate();
				pst.close();
			} else {
				java.sql.PreparedStatement pst;
				String insert="INSERT INTO persona (nombre,CP,pais,email) VALUES (\"Gonzalo\",\"28430\",\"España\",\"g.dlt@gmail.com\")";		
				pst = conexion.prepareStatement(insert);
				pst.executeUpdate();
				pst.close();
			}
		}
		
		rs.close();
		st.close();
	}

	private static void listadoPais(java.sql.Connection con) throws Exception {
		java.sql.Connection conexion = con;
		String selec="SELECT * FROM persona ORDER BY pais";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(selec);
		ArrayList<String>al=new ArrayList<String>();
		while(rs.next()) {
			al.add(rs.getString(1)+"   "+rs.getString(2)+"   "+rs.getString(3)+"   "+rs.getString(4));
		}
		rs.close();
		st.close();
		
		System.out.println("\nORDENADO POR PAIS:");
		for (String i : al) {
			System.out.println(i);
		}	
	}

	private static void listadoNombre(java.sql.Connection con) throws Exception {
		java.sql.Connection conexion = con;
		String selec="SELECT * FROM persona ORDER BY nombre";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(selec);
		ArrayList<String>al=new ArrayList<String>();
		while(rs.next()) {
			al.add(rs.getString(1)+"   "+rs.getString(2)+"   "+rs.getString(3)+"   "+rs.getString(4));
		}
		rs.close();
		st.close();
		
		System.out.println("\nORDENADO POR NOMBRE:");
		for (String i : al) {
			System.out.println(i);
		}
	}

	private static void listadoEmail(java.sql.Connection con) throws Exception {
		java.sql.Connection conexion = con;
		String selec="SELECT * FROM persona ORDER BY email";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(selec);
		ArrayList<String>al=new ArrayList<String>();
		while(rs.next()) {
			al.add(rs.getString(1)+"   "+rs.getString(2)+"   "+rs.getString(3)+"   "+rs.getString(4));
		}
		rs.close();
		st.close();
		
		System.out.println("\nORDENADO POR EMAIL:");
		for (String i : al) {
			System.out.println(i);
		}
	}

	private static void listadoCP(java.sql.Connection con) throws Exception {
		java.sql.Connection conexion = con;
		String selec="SELECT * FROM persona ORDER BY CP";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(selec);
		ArrayList<String>al=new ArrayList<String>();
		while(rs.next()) {
			al.add(rs.getString(1)+"   "+rs.getString(2)+"   "+rs.getString(3)+"   "+rs.getString(4));
		}
		rs.close();
		st.close();
		
		System.out.println("\nORDENADO POR CP:");
		for (String i : al) {
			System.out.println(i);
		}
		
	}

	private static void listadoNormal(java.sql.Connection con) throws Exception {
		java.sql.Connection conexion = con;
		String selec="SELECT * FROM persona";
		java.sql.Statement st = conexion.createStatement();
		ResultSet rs = st.executeQuery(selec);
		ArrayList<String>al=new ArrayList<String>();
		while(rs.next()) {
			al.add(rs.getString(1)+"   "+rs.getString(2)+"   "+rs.getString(3)+"   "+rs.getString(4));
		}
		rs.close();
		st.close();
		
		System.out.println("\nORDENADO NORMAL:");
		for (String i : al) {
			System.out.println(i);
		}
		
	}

	private static java.sql.Connection conectarBBDD() throws Exception {

		java.sql.Connection con = null;

		ArrayList<String> lineas = new ArrayList<>();
		String linea;
		File f = new File("CFG.INI");
		BufferedReader br = new BufferedReader(new FileReader(f));
		while ((linea = br.readLine()) != null) {
			lineas.add(linea);
		}
		br.close();

		String tipoBD = "mysql";
		String IP = lineas.get(2);
		String bd = lineas.get(1);
		String usu = lineas.get(3);
		String pass = lineas.get(4);

		return con = PersistenciaBD.PersistenciaBD(tipoBD, IP, usu, pass, bd);
	}

}
