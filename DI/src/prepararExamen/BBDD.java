package prepararExamen;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;

public class BBDD {

    static java.sql.Connection con = null;

    public static Connection conectarBBDD() throws SQLException {
        ArrayList<String> al = new ArrayList<String>();
        try {

            BufferedReader bfr = new BufferedReader(new FileReader(new File("CFG.INI")));
            String linea;
            while ((linea = bfr.readLine()) != null) {
                al.add(linea);
            }
            bfr.close();
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {
            System.err.println("No se encontró el Drive MySQL para JDBC.");
        }
        con = DriverManager.getConnection("jdbc:mysql://" + al.get(0), al.get(1), al.get(2));
        return con;
    }

}
