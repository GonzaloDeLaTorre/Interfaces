package prepararExamen;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;
import prepararExamen.Entrada;

public class ClasePrincipal {

    public static void main(String[] args) throws SQLException {

        java.sql.Connection con = null;

        con = conectarBD();

        //GENERAR SORTEO
        System.out.println("GENERAR SORTEO:");
        System.out.print("Año del sorteo: ");
        String año = Entrada.cadena();
        int condicionNegativa1 = 0;
        condicionNegativa1 = comprobarExisteBBDD(Integer.valueOf(año));
        if (condicionNegativa1 == 1) {
            System.out.println("Ya existe la bbdd ¿Borrar bbdd antigua y crear una nueva? Responde: SI ó NO");
            String respuesta = Entrada.cadena();
            if (respuesta.equals("NO")) {
                //COMPROBAR SORTEO
                System.out.println("\n\nUSUARIO:");
                System.out.print("Año del sorteo: ");
                String año1 = Entrada.cadena();
                condicionNegativa1 = 0;
                condicionNegativa1 = comprobarExisteBBDD(Integer.valueOf(año1));
                if (condicionNegativa1 == 0) {
                    System.out.println("No existe el sorteo para el año " + año + ".");
                } else {
                    int condicionNegativa = 0;
                    condicionNegativa = comprobarAño(Integer.valueOf(año1));
                    if (condicionNegativa == 0) {
                        System.out.println("No existe la BBDD.");
                    } else {
                        System.out.println("Comprobar número premiado: ");
                        int numero = Entrada.entero();
                        comprobarNumero(numero, Integer.valueOf(año1));
                    }
                }
                System.exit(-1);
            } else {
                borrarBBDD(año);
                generarAño(año);
                crearTabla(año);

                System.out.print("Cuantos números distintos desea generar: ");
                int numeros = Entrada.entero();
                insertarFilas(año, numeros);
            }
        } else {
            generarAño(año);
            crearTabla(año);

            System.out.print("Cuantos números: ");
            int numeros = Entrada.entero();
            insertarFilas(año, numeros);
        }

        //COMPROBAR SORTEO
        System.out.println("\n\nUSUARIO:");
        System.out.print("Año del sorteo: ");
        String año1 = Entrada.cadena();
        condicionNegativa1 = 0;
        condicionNegativa1 = comprobarExisteBBDD(Integer.valueOf(año1));
        if (condicionNegativa1 == 0) {
            System.out.println("No existe el sorteo para el año " + año + ".");
        } else {
            int condicionNegativa = 0;
            condicionNegativa = comprobarAño(Integer.valueOf(año1));
            if (condicionNegativa == 0) {
                System.out.println("No existe la BBDD.");
            } else {
                System.out.print("Comprobar número premiado: ");
                int numero = Entrada.entero();
                comprobarNumero(numero, Integer.valueOf(año1));
            }
        }

    }

    private static void borrarBBDD(String año) throws SQLException {
        java.sql.Connection con = null;
        con = conectarBD();

        String borrarBBDD = "DROP DATABASE navidad" + año + ";";
        PreparedStatement ps = con.prepareStatement(borrarBBDD);
        ps.executeUpdate();
    }

    private static int comprobarExisteBBDD(int añobd) throws SQLException {
        java.sql.Connection con = null;
        con = conectarBD();

        Statement st = con.createStatement();
        ResultSet rs = con.getMetaData().getCatalogs();

        int condicionNegativa = 0;
        while (rs.next()) {
            if (rs.getString("TABLE_CAT").equals("navidad" + añobd)) {
                condicionNegativa = 1;
            }
        }
        return condicionNegativa;
    }

    private static void comprobarNumero(int numero, int añobd) throws SQLException {
        java.sql.Connection con = null;
        con = conectarBD();

        String usarBBDD = "USE navidad" + añobd + ";";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(usarBBDD);

        String buscar = "SELECT * FROM premiados;";
        Statement st2 = con.createStatement();
        ResultSet rs2 = st2.executeQuery(buscar);

        int condicionNegativa = 0;
        while (rs2.next()) {
            if (rs2.getString(1).equals(String.valueOf(numero))) {
                System.out.println("Número premiado. El número " + numero + " tiene un premio de " + rs2.getString(2) + ".");
                condicionNegativa = 1;
            }
        }
        if (condicionNegativa != 1) {
            System.out.println("Número no premiado.");
        }
    }

    private static int comprobarAño(int añobd) throws SQLException {
        java.sql.Connection con = null;
        con = conectarBD();

        Statement st = con.createStatement();
        ResultSet rs = con.getMetaData().getCatalogs();

        int condicionNegativa = 0;
        while (rs.next()) {
            if (rs.getString("TABLE_CAT").equals("navidad" + añobd)) {
                System.out.println("BBDD encontrada.");
                System.out.println(rs.getString("TABLE_CAT"));
                condicionNegativa = 1;
            }
        }
        return condicionNegativa;
    }

    private static void insertarFilas(String año, int numeros) throws SQLException {
        java.sql.Connection con = null;
        con = conectarBD();

        String usarBBDD = "USE navidad" + año + ";";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(usarBBDD);

        int n1 = 0;
        int n2 = 0;
        for (int i = 0; i < numeros; i++) {
            n1 = generarAleatorios(numeros);
            n2 = generarPremio();
            String crearTabla = "INSERT INTO premiados (numero, premio) VALUES ('" + n1 + "', '" + n2 + "')";
            PreparedStatement ps = con.prepareStatement(crearTabla);
            ps.executeUpdate();
        }
    }

    private static int generarPremio() {
        // NO ME SALE!! NO RECUERDO ESTA PARTE DE LA CLASE RANDOM
        int p1 = 1000;
        int p2 = 25000;
        int p3 = 50000;
        int p4 = 100000;

        ArrayList<Integer> al = new ArrayList<Integer>();
        al.add(p1);
        al.add(p2);
        al.add(p3);
        al.add(p4);
        Random r = new Random();
        int res = r.nextInt(p1);

        return res;
    }

    private static int generarAleatorios(int numeros) {
        Random r = new Random();
        int numAleatorio = r.nextInt(99999);
        return numAleatorio;
    }

    private static void crearTabla(String año) throws SQLException {
        java.sql.Connection con = null;
        con = conectarBD();

        String usarBBDD = "USE navidad" + año + ";";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(usarBBDD);

        String crearTabla = "CREATE TABLE premiados (numero INTEGER PRIMARY KEY, premio INTEGER)";
        PreparedStatement ps = con.prepareStatement(crearTabla);
        ps.executeUpdate();
    }

    public static void generarAño(String año) throws SQLException {
        java.sql.Connection con = null;
        con = conectarBD();

        String crearBBDD = "CREATE DATABASE navidad" + año;
        PreparedStatement ps = con.prepareStatement(crearBBDD);
        ps.executeUpdate();
    }

    private static Connection conectarBD() {
        java.sql.Connection con = null;
        try {
            con = BBDD.conectarBBDD();
        } catch (SQLException e) {
            System.err.println("No se ha conectado a la BBDD.");
        }
        return con;
    }

}
