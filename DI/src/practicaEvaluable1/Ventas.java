/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaEvaluable1;

import java.util.ArrayList;

/**
 *
 * @author alumno
 */
public class Ventas extends practicaEvaluable1.Formulario {

        String nombre;
	int localidad;
	int procesador;
	int memoria;
	int monitor;
	int discoDuro;
	boolean grabadoraDvd;
	boolean wifi;
	boolean sintonizadorTv;
	boolean backUpRestore;
	

	public Ventas(String nombre, int localidad, int procesador, int memoria, int monitor, int discoDuro, boolean grabadoraDvd, boolean wifi, boolean sintonizadorTv, boolean backUpRestore) {
                this.nombre = nombre;		
                this.localidad = localidad;
		this.procesador = procesador;
		this.memoria = memoria;
		this.monitor = monitor;
		this.discoDuro = discoDuro;
		this.grabadoraDvd = grabadoraDvd;
		this.wifi = wifi;
		this.sintonizadorTv = sintonizadorTv;
		this.backUpRestore = backUpRestore;
	}

        
        public String getNombre() {
            return nombre;
        }

        public void setNombre(String nombre) {
            this.nombre = nombre;
        }
        
	public int getLocalidad() {
		return localidad;
	}

	public void setLocalidad(int localidad) {
		this.localidad = localidad;
	}

	public int getProcesador() {
		return procesador;
	}

	public void setProcesador(int procesador) {
		this.procesador = procesador;
	}

	public int getMemoria() {
		return memoria;
	}

	public void setMemoria(int memoria) {
		this.memoria = memoria;
	}

	public int getMonitor() {
		return monitor;
	}

	public void setMonitor(int monitor) {
		this.monitor = monitor;
	}

	public int getDiscoDuro() {
		return discoDuro;
	}

	public void setDiscoDuro(int discoDuro) {
		this.discoDuro = discoDuro;
	}

	public boolean getGrabadoraDvd() {
		return grabadoraDvd;
	}

	public void setGrabadoraDvd(boolean grabadoraDvd) {
		this.grabadoraDvd = grabadoraDvd;
	}

	public boolean getWifi() {
		return wifi;
	}

	public void setWifi(boolean wifi) {
		this.wifi = wifi;
	}

	public boolean getSintonizadorTv() {
		return sintonizadorTv;
	}

	public void setSintonizadorTv(boolean sintonizadorTv) {
		this.sintonizadorTv = sintonizadorTv;
	}

	public boolean getBackUpRestore() {
		return backUpRestore;
	}

	public void setBackUpRestore(boolean backUpRestore) {
		this.backUpRestore = backUpRestore;
	}

	
}
