create database if not exists interfaces;
use interfaces;
create table if not exists clientes (
codigo varchar(6) primary key,
nif varchar(9) not null,
apellidos varchar(35) not null,
nombre varchar(15) not null,
domicilio varchar(40) not null,
codigoPostal varchar(5) not null,
localidad varchar(20) not null,
telefono varchar(9),
movil varchar(9),
fax varchar(9),
email varchar(20) not null,
totalVentas float not null
);