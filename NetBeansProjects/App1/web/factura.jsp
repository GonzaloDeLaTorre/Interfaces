<%-- 
    Document   : factura
    Created on : 14-feb-2020, 0:58:47
    Author     : Gonzalo
--%>

<%@page import="java.util.ArrayList"%>
<%@page import="practicaEvaluable3.Articulos"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
</head>

<%
    //CONECTANDO A LA BASE DE DATOS:
    Connection con;
    String url = "jdbc:mysql://localhost/interfaces";
    String Driver = "com.mysql.jdbc.Driver";
    String user = "root";
    String password = ""; //manager en clase, vacío en casa
    Class.forName(Driver);
    con = DriverManager.getConnection(url, user, password);
    //Listar Datos de la Tabla Clientes:
    HttpSession sesion = request.getSession(); // Abro sesion
    Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la anterior pág.
%>
<hr size="2px" color="grey"/>
<%
    java.util.Date d = new java.util.Date();
    java.sql.Date date2 = new java.sql.Date(d.getTime());
    SimpleDateFormat plantilla = new SimpleDateFormat("dd-MM-yyyy");
    String tiempo = plantilla.format(date2);

    int cnt1 = (Integer) sesion.getAttribute("cnt1");
%>
Fecha:  <%= tiempo%><br>
FACTURA N: <%= cnt1%>

<hr size="0.5px" color="grey"/>

<h1 style="text-align: center"><b>Empresa Proyecto Web de clase, S.A.</h1>
<h2 style="text-align: center">N.I.F: 28938475-J</h2>
<h3 style="text-align: center">C/ Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid</b></h3>

<hr size="0.5px" color="grey"/>
<p>
    <b>Cliente:</b> <%= c.getCodigo()%> <b>N.I.F.:</b> <%= c.getNif()%><br>
    <b>D./Dña.</b> <%= c.getNombre()%> <%= c.getApellidos()%><br>
    <%= c.getDomicilio()%><br>
    <%= c.getCp()%> <%= c.getLocalidad()%>
</p>
<hr size="0.5px" color="grey"/>
<p>
    <%
        ArrayList<Articulos> al = (ArrayList) sesion.getAttribute("ar");
    %>
<div style="text-align:center;">
    <table style="margin: 0 auto;" id="tablaDatos"> <%-- class="table table-bordered" --%>
        <thead> <%-- Cabecera donde van a ir los titulos: --%>
            <tr> <%-- fila: --%>
                <th>Artículo</th> <%-- columna: --%>
                <th>Descripción</th>
                <th>Unidades</th>
                <th>Precio</th>
                <th>Importe</th>
            </tr>
        </thead>
        <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los articulos: --%> <%-- NO TIENE CIERRE --%>
            <%
                float cnt = 0;
                for (int f = 0; f < al.size(); f++) {%>
            <tr>
                <td><%= al.get(f).getCodigo()%></td>
                <td><%= al.get(f).getDescripcion()%></td>
                <td><%= al.get(f).getUnidades()%></td>
                <td><%= al.get(f).getPrecio()%></td>
                <td><%= al.get(f).getImporte()%></td><p>
            </tr> 
            <%
                    cnt = cnt + al.get(f).getImporte();
                }
            %>  
    </table>
</div>
<hr size="0.5px" color="grey"/>
<h1 style="text-align: right"><b>Total Factura (I.V.A. inc.): <%= cnt%></b></h1>
<hr size="2px" color="grey"/>
</html>
