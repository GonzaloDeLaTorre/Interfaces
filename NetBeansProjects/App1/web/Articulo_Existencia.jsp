<%-- 
    Document   : sexta
    Created on : 12-feb-2020, 9:27:01
    Author     : alumno
--%>

<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="practicaEvaluable3.Articulos"%>
<%@page import="java.util.ArrayList"%>
<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de Pedidos</title>
    </head>
    <body>
        <h1>Gestión de Pedidos</h1>
        <%
            //CONECTANDO A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost/interfaces";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String password = ""; //manager en clase, vacío en casa
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, password);
            //Listar Datos de la Tabla Clientes:
            HttpSession sesion = request.getSession(); // Abro sesion
            Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la anterior pág.
            String cod_Articulo = request.getParameter("txtArticulo");
            if (Metodos.buscarArticulo(con, cod_Articulo) == true) { // Si el articulo existe relleno clientes, y después articulo
%>
        <h2>Datos del cliente</h2>
        <%-- Creamos la Tabla: --%>
        <table style="text-align:left;" id="tablaDatos"> <%-- class="table table-bordered" --%>
            <thead> <%-- Cabecera donde van a ir los titulos: --%>
                <tr> <%-- fila: --%>
                    <th style="text-align:left;">Código</th> <%-- columna: --%>
                    <th style="text-align:left;">N.I.F.</th>
                    <th style="text-align:left;">Nombre</th>
                    <th style="text-align:left;">Apellidos</th>
                </tr>
            </thead>
            <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los clientes: --%> <%-- NO TIENE CIERRE --%>
                <tr>
                    <td><%= c.getCodigo() /*rs.getString("codigo")*/%></td>
                    <td><%= c.getNif()%></td>
                    <td><%= c.getNombre()%></td>
                    <td><%= c.getApellidos()%></td>
                </tr> 
            <thead>
                <tr>
                    <th style="text-align:left;">Domicilio</th>
                    <th style="text-align:left;">C.P.</th>
                    <th style="text-align:left;">Localidad</th>
                    <th style="text-align:left;">Total</th>
                </tr>
            </thead>
            <tr>
                <td><%= c.getDomicilio()%></td>
                <td><%= c.getCp()%></td>
                <td><%= c.getLocalidad()%></td>
                <td>0</td>
            </tr>
        </table>

        <hr size="2px" color="grey"/> <%-- Línea separadora --%>

        <%
            //Listar Datos de la Tabla Artículos:
            Articulos a = Metodos.añadirArticulo(con, cod_Articulo); // Creo el cliente
            sesion.setAttribute("Articulo", a); // Guardo el cliente creado en la sesion para utilizarlo después.
        %>
        <h2>Realizar pedido</h2>
        <form action="Aceptar_Pedido.jsp" method="post" id="formulario" name="formPedidos" onsubmit="return enviar()">
            <table style="text-align:left;" id="tablaDatos"> <%-- class="table table-bordered" --%>
                <thead> <%-- Cabecera donde van a ir los titulos: --%>
                    <tr> <%-- fila: --%>
                        <th>Artículo</th> <%-- columna: --%>
                        <th>Descripción</th>
                        <th>Unidades</th>
                        <th>Precio</th>
                        <th>Importe</th>
                    </tr>
                </thead>
                <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los articulos: --%> <%-- NO TIENE CIERRE --%>
                    <tr>
                        <td><%= a.getCodigo()%></td>
                        <td><%= a.getDescripcion()%></td>
                        <td><input type="number" name="txtUnidades" id="cajaUnidades" onkeyup="CalcularPrecio(event)" autofocus="" pattern="[0-9]{0,6}" maxlength="6" size="6" title="Solo se admiten de 1 a 6 dígitos alfanuméricos."></td>
                        <td><%= a.getPrecio()%></td>
                        <td><input type="text" name="txtImporte" readonly="" maxlength="6" size="6"></td><p>
                    </tr> 
            </table>

            <input type="hidden" name="txtPrecio" value="<%= a.getPrecio()%>"> <%-- Necesario para actualizar el importe --%>

            <input type="submit" value="Aceptar" accesskey="A"> <!-- No hay que poner onClick puesto que lo recoge en su predefino que ponemos en el form -> onsubmit -->
            <input type="reset" value="Cancelar" onclick="reset2()" accesskey="C"><p>
        </form>

        <hr size="2px" color="grey"/>

        <a href="Cliente_Existencia.jsp">Nuevo artículo</a> | <a href="index.jsp">Página principal</a>
        <%
        } else {
        %>
        <h2>El artículo con código <%= request.getParameter("txtArticulo")%> no existe</h2><p>

            <a href="Cliente_Existencia.jsp">Nuevo artículo</a> | <a href="index.jsp">Página principal</a>
            <%
                } // Cierro el if
%>
    </body>
    <script language="javascript" type="text/javascript"> // Aquí tenemos las funciones que llamamos en los botones			}
        function enviar() {
            with (document.getElementById("cajaUnidades")) {
                if (document.getElementById("cajaUnidades").value === "" || document.getElementById("cajaUnidades").value === "0" || document.getElementById("cajaUnidades").value === "00" || document.getElementById("cajaUnidades").value === "000" || document.getElementById("cajaUnidades").value === "0000" || document.getElementById("cajaUnidades").value === "00000" || document.getElementById("cajaUnidades").value === "000000") { // Si la caja de Artículo está vacía
                    alert("Debes introducir un dato.");
                    document.getElementById("cajaUnidades").focus();
                    return false;
                }
            }
        }
        function reset2() {
            document.getElementById("cajaUnidades").focus();
        }
        function CalcularPrecio(event) {
            with (document.formPedidos.txtUnidades) {
                if (event.keyCode > 64 && event.keyCode < 91 || event.keyCode === 32) {
                    alert("Introduce solo números");
                    value = "";
                } else {
                    var importe = value * document.formPedidos.txtPrecio.value;
                    document.formPedidos.txtImporte.value = importe;
                }
            }
        }
    </script>
</html>
