<%-- 
    Document   : cuarta
    Created on : 06-feb-2020, 11:25:31
    Author     : alumno
--%>
<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de Pedidos</title>
    </head>
    <body>
        <h1>Gestión de Pedidos</h1>
        <%
            //CONECTANDO A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost/interfaces";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String password = ""; //manager en clase, vacío en casa
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, password);

            //Listar Datos de la Tabla Clientes:
            HttpSession sesion = request.getSession(); // Abro sesion
            Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la siguiente pág.
            String cod_Cliente = request.getParameter("txtCodigo"); // Recojo código del cliente enviado de la anterior pág.
            if (cod_Cliente == null) {
                cod_Cliente = c.getCodigo();
            }
            if (Metodos.buscarCliente(con, cod_Cliente) == true) { // Compruebo si existe el cliente, en el caso que SÍ entra en el if.
                c = Metodos.añadirCliente(con, cod_Cliente); // Creo cliente y le Añado valores
                sesion.setAttribute("Cliente", c); // Guardo el cliente creado en la sesion para utilizarlo después.
%>

        <h2>Datos del cliente</h2>
        <%-- Creamos la Tabla: --%>
        <table style="text-align:left;" id="tablaDatos"> <%-- class="table table-bordered" --%>
            <thead> <%-- Cabecera donde van a ir los titulos: --%>
                <tr> <%-- fila: --%>
                    <th style="text-align:left;">Código</th> <%-- columna: --%>
                    <th style="text-align:left;">N.I.F.</th>
                    <th style="text-align:left;">Nombre</th>
                    <th style="text-align:left;">Apellidos</th>
                </tr>
            </thead>
            <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los clientes: --%> <%-- NO TIENE CIERRE --%>
                <tr>
                    <td><%= c.getCodigo() /*rs.getString("codigo")*/%></td>
                    <td><%= c.getNif()%></td>
                    <td><%= c.getNombre()%></td>
                    <td><%= c.getApellidos()%></td>
                </tr> 
            <thead>
                <tr>
                    <th style="text-align:left;">Domicilio</th>
                    <th style="text-align:left;">C.P.</th>
                    <th style="text-align:left;">Localidad</th>
                    <th style="text-align:left;">Total</th>
                </tr>
            </thead>
            <tr>
                <td><%= c.getDomicilio()%></td>
                <td><%= c.getCp()%></td>
                <td><%= c.getLocalidad()%></td>
                <td>0</td>
            </tr>
        </table>

        <hr size="2px" color="grey"/> <%-- Línea separadora --%>

        <h2>Realizar pedido</h2>
        <form action="Articulo_Existencia.jsp" method="post" id="formulario" onsubmit="return enviar()">
            Artículo<br> <input type="text" name="txtArticulo" id="cajaArticulo" autofocus="" pattern="[A-z0-9]{0,6}" maxlength="6" size="6" title="Solo se admiten de 1 a 6 dígitos alfanuméricos."><p>

                <input type="submit" value="Aceptar" accesskey="A"> <!-- No hay que poner onClick puesto que lo recoge en su predefino que ponemos en el form -> onsubmit -->
            <input type="reset" value="Cancelar" onclick="reset2()" accesskey="C"><p>
        </form>

        <hr size="2px" color="grey"/>

        <a href="segunda.jsp">Nuevo cliente</a> | <a href="index.jsp">Página principal</a>


        <%
        } else { // Cliente no existe
%>

        <h2>El cliente con código <%= request.getParameter("txtCodigo")%> no existe</h2><p>

            <a href="segunda.jsp">Nuevo cliente</a> | <a href="index.jsp">Página principal</a>
            <%
                }
            %>
    </body>
    <script> // Aquí tenemos las funciones que llamamos en los botones			}
        function enviar() {
            with (document.getElementById("cajaArticulo")) {
                if (document.getElementById("cajaArticulo").value === "") { // Si la caja de Artícyulo está vacía
                    alert("Debes introducir un dato.");
                    document.getElementById("cajaArticulo").focus();
                    return false;
                } else {
                    for (i = 1; value.length < 6; i++) {
                        value = "0" + value;
                    }
                }
            }
        }
        function reset2() {
            document.getElementById("cajaArticulo").focus();
        }
    </script>
</html>
