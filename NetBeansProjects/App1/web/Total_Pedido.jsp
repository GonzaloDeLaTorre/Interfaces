<%-- 
    Document   : Total_Pedido
    Created on : 12-feb-2020, 19:27:44
    Author     : Gonzalo
--%>

<%@page import="practicaEvaluable3.PedidosWeb"%>
<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="practicaEvaluable3.Articulos"%>
<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de pedidos</title>
    </head>

    <%
        //CONECTANDO A LA BASE DE DATOS:
        Connection con;
        String url = "jdbc:mysql://localhost/interfaces";
        String Driver = "com.mysql.jdbc.Driver";
        String user = "root";
        String password = ""; //manager en clase, vacío en casa
        Class.forName(Driver);
        con = DriverManager.getConnection(url, user, password);
        //Listar Datos de la Tabla Clientes:
        HttpSession sesion = request.getSession(); // Abro sesion
        Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la anterior pág.
    %>
    <h1>Pedido finalizado</h1>
    <p>
        Si desea imprimir el pedido se abrirá una nueva ventana con la factura a imprimir.<br>
        En esta ventana, abra el menú Archivo y ejecute la opción Imprimir.<br>
        En la nueva ventana, seleccione su impresora y pulse Imprimir.<br>
        Después, cierre la ventana que contiene la factura a Imprimir.<br>
        Para imprimir el pedido pulse <a href="factura.jsp" target="_blank">aquí</a>. Si no va a imprimir, puede regresar a la <a href="index.jsp">página principal</a>.
    </p>
    <hr size="2px" color="grey"/>
    <%
        java.util.Date d = new java.util.Date();
        java.sql.Date date2 = new java.sql.Date(d.getTime());
        SimpleDateFormat plantilla = new SimpleDateFormat("dd-MM-yyyy");
        String tiempo = plantilla.format(date2);

        int cnt1;
        if (Metodos.buscarVenta(con) == true) {
            if (sesion.getAttribute("cnt1") == null) {
                cnt1 = 1;
                sesion.setAttribute("cnt1", cnt1);
            } else {
                cnt1 = (Integer) sesion.getAttribute("cnt1");
                cnt1++;
                sesion.setAttribute("cnt1", cnt1);
            }
        } else {
            cnt1 = 1;
            sesion.setAttribute("cnt1", cnt1);
        }
    %>
    Fecha:  <%= tiempo%><br>
    FACTURA N: <%= cnt1%>

    <hr size="0.5px" color="grey"/>

    <h1 style="text-align: center"><b>Empresa Proyecto Web de clase, S.A.</h1>
    <h2 style="text-align: center">N.I.F: 28938475-J</h2>
    <h3 style="text-align: center">C/ Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid</b></h3>

<hr size="0.5px" color="grey"/>
<p>
    <b>Cliente:</b> <%= c.getCodigo()%> <b>N.I.F.:</b> <%= c.getNif()%><br>
    <b>D./Dña.</b> <%= c.getNombre()%> <%= c.getApellidos()%><br>
    <%= c.getDomicilio()%><br>
    <%= c.getCp()%> <%= c.getLocalidad()%>
</p>
<hr size="0.5px" color="grey"/>
<p>
    <%
        ArrayList<Articulos> al = (ArrayList) sesion.getAttribute("ar");

        for (int i = 0; i < al.size(); i++) {
            String cliente = c.getCodigo();
            String articulo = al.get(i).getCodigo();
            float unidades = al.get(i).getUnidades();
            Metodos.guardarPedido(con, cliente, articulo, unidades);
        }
    %>
<div style="text-align:center;">
    <table style="margin: 0 auto;" id="tablaDatos"> <%-- class="table table-bordered" --%>
        <thead> <%-- Cabecera donde van a ir los titulos: --%>
            <tr> <%-- fila: --%>
                <th>Artículo</th> <%-- columna: --%>
                <th>Descripción</th>
                <th>Unidades</th>
                <th>Precio</th>
                <th>Importe</th>
            </tr>
        </thead>
        <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los articulos: --%> <%-- NO TIENE CIERRE --%>
            <%
                float cnt = 0;
                for (int f = 0; f < al.size(); f++) {%>
            <tr>
                <td><%= al.get(f).getCodigo()%></td>
                <td><%= al.get(f).getDescripcion()%></td>
                <td><%= al.get(f).getUnidades()%></td>
                <td><%= al.get(f).getPrecio()%></td>
                <td><%= al.get(f).getImporte()%></td><p>
            </tr> 
            <%
                    cnt = cnt + al.get(f).getImporte();
                }
                float tv = c.getTotal_ventas() + (Float) (cnt);
                Metodos.guardarTotalVenta(con, c.getCodigo(), tv);
            %>  
    </table>
</div>
<hr size="0.5px" color="grey"/>
<h1 style="text-align: right"><b>Total Factura (I.V.A. inc.): <%= cnt%></b></h1>
<hr size="2px" color="grey"/>
<p></p>
</html>
