<%-- 
    Document   : prueba
    Created on : 07-feb-2020, 11:11:15
    Author     : alumno
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
		<%
            //CONECTANOD A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost/interfaces";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String password = "manager";
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, password);
			//Listar Datos de la Tabla Clientes
            PreparedStatement ps;
            Statement st;
            ResultSet rs;
            st = con.createStatement();
            rs = st.executeQuery("select * from clientes"); 
        %>
		<%-- Creamos la Tabla: --%>
		<table class="table table-bordered"  id="tablaDatos">
                    <thead> <%-- Cabecera donde van a ir los titulos: --%>
                        <tr> <%-- fila: --%>
                            <th>Código</th> <%-- columna: --%>
                            <th>N.I.F.</th>
                            <th>Nombre</th>
							<th>Apellidos</th>
						</tr>
                    </thead>
					<tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los clientes: --%> <%-- NO TIENE CIERRE --%>
                        <%
                            while (rs.next()) { // While para leer los datos
                        %>
                        <tr>
                            <td class="text-center"><%= rs.getString("codigo")%></td>
                            <td class="text-center"><%= rs.getString("nif")%></td>
                            <td class="text-center"><%= rs.getString("nombre")%></td>
							<td class="text-center"><%= rs.getString("apellidos")%></td>
						</tr> 
					<thead>
						<tr>
							<th>Domicilio</th>
                            <th>C.P.</th>
                            <th>Localidad</th>
							<th>Total</th>
                        </tr>
                    </thead>
						<tr>
							<td class="text-center"><%= rs.getString("domicilio")%></td>
							<td class="text-center"><%= rs.getString("cp")%></td>
							<td class="text-center"><%= rs.getString("localidad")%></td>
							<td class="text-center"><%= rs.getString("total_ventas")%></td>
                        </tr>
						<%}%> <%--Cerramos while --%>
                </table>
    </body>
</html>
