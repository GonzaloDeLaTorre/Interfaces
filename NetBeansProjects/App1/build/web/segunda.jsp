<%-- 
    Document   : segunda
    Created on : 31-ene-2020, 11:43:39
    Author     : alumno
--%>
<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de Pedidos</title>
    </head>
    <body>
        <h1>Gestión de Pedidos</h1>
		<!-- Manera de comentar -->
		<!-- para que todos los objetos esten en la misma linea, poner en el formulario   style="display:inline;"-->
        <!--'br' salto de línea, 'p' salto de párrafo-->
        <form action="Cliente_Existencia.jsp" method="post" id="formulario" onsubmit="return enviar()">
            Código de cliente <input type="text" name="txtCodigo" id="cajaCodigo" autofocus="" pattern="[A-z0-9]{0,6}" maxlength="6" size="6" title="Solo se admiten de 1 a 6 dígitos alfanuméricos."><p>

			<input type="submit" value="Aceptar" accesskey="A"> <!-- No hay que poner onClick puesto que lo recoge en su predefino que ponemos en el form -> onsubmit -->
			<input type="reset" value="Cancelar" onclick="reset2()" accesskey="C"><p>
        </form>

		<!--action="tercera.jsp?varForm=" method="post"-->

		<a href="index.jsp">Página principal</a>
    </body>
    <script> // Aquí tenemos las funciones que llamamos en los botones			}
		function enviar() {
			with (document.getElementById("cajaCodigo")) {
				if (document.getElementById("cajaCodigo").value === "") { // Si la caja de Código está vacía
					alert("Debes introducir un dato.");
					document.getElementById("cajaCodigo").focus();
					return false;
				} else {
					for (i = 1; value.length < 6; i++) {
						value = "0" + value;
					}
				}
			}
		}
		function reset2() {
//			document.getElementById("formulario").reset();
			document.getElementById("cajaCodigo").focus();
		}
    </script>
</html>
