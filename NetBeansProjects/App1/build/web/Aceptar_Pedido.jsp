<%-- 
    Document   : Aceptar_Pedido
    Created on : 12-feb-2020, 18:44:49
    Author     : Gonzalo
--%>

<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="practicaEvaluable3.Articulos"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Gestión de Pedidos</h1>
        <%                    
            //CONECTANDO A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost/interfaces";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String password = ""; //manager en clase, vacío en casa
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, password);
            //Listar Datos de la Tabla Clientes:
            HttpSession sesion = request.getSession(); // Abro sesion
            Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la anterior pág.
        %>
        <h2>Datos del cliente</h2>
        <%-- Creamos la Tabla: --%>
        <table style="text-align:left;" id="tablaDatos"> <%-- class="table table-bordered" --%>
            <thead> <%-- Cabecera donde van a ir los titulos: --%>
                <tr> <%-- fila: --%>
                    <th style="text-align:left;">Código</th> <%-- columna: --%>
                    <th style="text-align:left;">N.I.F.</th>
                    <th style="text-align:left;">Nombre</th>
                    <th style="text-align:left;">Apellidos</th>
                </tr>
            </thead>
            <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los clientes: --%> <%-- NO TIENE CIERRE --%>
                <tr>
                    <td><%= c.getCodigo() /*rs.getString("codigo")*/%></td>
                    <td><%= c.getNif()%></td>
                    <td><%= c.getNombre()%></td>
                    <td><%= c.getApellidos()%></td>
                </tr> 
            <thead>
                <tr>
                    <th style="text-align:left;">Domicilio</th>
                    <th style="text-align:left;">C.P.</th>
                    <th style="text-align:left;">Localidad</th>
                    <th style="text-align:left;">Total</th>
                </tr>
            </thead>
            <tr>
                <td><%= c.getDomicilio()%></td>
                <td><%= c.getCp() /*rs.getString("cp")*/%></td>
                <td><%= c.getLocalidad()%></td>
                <td><%= c.getTotal_ventas()%></td>
            </tr>
        </table>

        <hr size="2px" color="grey"/> <%-- Línea separadora --%>

        <%
            ArrayList <Articulos> al = (ArrayList) sesion.getAttribute("ar");
            //Listar Datos de la Tabla Artíuclos:            
            Articulos a = (Articulos) sesion.getAttribute("Articulo"); // Recojo cliente de la anterior pág.
            String u = request.getParameter("txtUnidades");
            String i = request.getParameter("txtImporte");
            a = new Articulos(a.getCodigo(), a.getDescripcion(), Float.parseFloat(u), a.getPrecio(), Float.parseFloat(i));
            if(al == null){
                al = new ArrayList<Articulos>();
            }
            al.add(a);
            sesion.setAttribute("ar", al); // Guardo el cliente creado en la sesion para utilizarlo después.
        %>
        <h2>Pedido realizado</h2>
        <form action="Total_Pedido.jsp" method="post" id="formulario" onsubmit="return enviar()">
            <table style="text-align:left;" id="tablaDatos"> <%-- class="table table-bordered" --%>
                <thead> <%-- Cabecera donde van a ir los titulos: --%>
                    <tr> <%-- fila: --%>
                        <th>Artículo</th> <%-- columna: --%>
                        <th>Descripción</th>
                        <th>Unidades</th>
                        <th>Precio</th>
                        <th>Importe</th>
                    </tr>
                </thead>
                <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los articulos: --%> <%-- NO TIENE CIERRE --%>
                     <% 
                        float cnt = 0;
                        for(int f = 0; f < al.size(); f++){ %>
                    <tr>
                        <td><%= al.get(f).getCodigo()%></td>
                        <td><%= al.get(f).getDescripcion()%></td>
                        <td><%= al.get(f).getUnidades()%></td>
                        <td><%= al.get(f).getPrecio()%></td>
                        <td><%= al.get(f).getImporte()%></td><p>
                    </tr> 
                  <% 
                        cnt = cnt + al.get(f).getImporte();
                        }
                   %>  
            </table>
        <h2>Importe del pedido: <%= cnt %></h2>
        
        <hr size="2px" color="grey"/>
        
        <input type="submit" value="Aceptar Pedido" accesskey="A"><p> <!-- No hay que poner onClick puesto que lo recoge en su predefino que ponemos en el form -> onsubmit -->
        </form>
        
        <a href="Cliente_Existencia.jsp">Pedir otro artículo</a> | <a href="index.jsp">Página principal</a>
    </body>
</html>
