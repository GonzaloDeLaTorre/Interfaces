<%-- 
    Document   : Total_Pedido
    Created on : 12-feb-2020, 19:27:44
    Author     : Gonzalo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de pedidos</title>
    </head>
    <body>
        <h1>Pedido finalizado</h1>
        <p>
            Si desea imprimir el pedido se abrirá una nueva ventana con la factura a imprimir.<br>
            En esta ventana, abra el menú Archivo y ejecute la opción Imprimir.<br>
            En la nueva ventana, seleccione su impresora y pulse Imprimir.<br>
            Después, cierre la ventana que contiene la factura a Imprimir.<br>
            Para imprimir el pedido pulse <a href="factura.jsp">aquí</a>. Si no va a imprimir, puede regresar a la <a href="index.jsp">página principal</a>.
        </p>
        <hr size="2px" color="grey"/>
        <p>
            Fecha: <br>
            FACTURA N: 
        </p>
        <hr size="0.5px" color="grey"/>
        <p>
            Empresa Proyecto Web de clase, S.A.<br>
            N.I.F: 28938475-J<br>
            C/Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid
        </p>
        <hr size="0.5px" color="grey"/>
        <p>
           Cliente: <br>
           D./Dña. <br>
           ...<br>
           ...
        </p>
        <hr size="0.5px" color="grey"/>
        <p>
           Codigo Descripción....<br>TABLA
        </p>
        <hr size="0.5px" color="grey"/>
        <p>
           Total Factura (I.V.A. inc.): IMPORTEEE
        </p>
        <hr size="2px" color="grey"/>
    </body>
</html>
