/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaEvaluable3;

/**
 *
 * @author alumno
 */
public class Cliente {
	String codigo;
    String nif;
    String apellidos;
    String nombre;
    String domicilio;
	String cp;
	String localidad;
	float total_ventas;

	public Cliente(String codigo, String nif, String apellidos, String nombre, String domicilio, String cp, String localidad, float total_ventas) {
		this.codigo = codigo;
		this.nif = nif;
		this.apellidos = apellidos;
		this.nombre = nombre;
		this.domicilio = domicilio;
		this.cp = cp;
		this.localidad = localidad;
		this.total_ventas = total_ventas;
	}

	@Override
	public String toString() {
		return "Cliente{" + "codigo=" + codigo + ", nif=" + nif + ", apellidos=" + apellidos + ", nombre=" + nombre + ", domicilio=" + domicilio + ", cp=" + cp + ", localidad=" + localidad + ", total_ventas=" + total_ventas + '}';
	}
	
	

	Cliente(String string) {
		
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNif() {
		return nif;
	}

	public void setNif(String nif) {
		this.nif = nif;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDomicilio() {
		return domicilio;
	}

	public void setDomicilio(String domicilio) {
		this.domicilio = domicilio;
	}

	public String getCp() {
		return cp;
	}

	public void setCp(String cp) {
		this.cp = cp;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public float getTotal_ventas() {
		return total_ventas;
	}

	public void setTotal_ventas(float total_ventas) {
		this.total_ventas = total_ventas;
	}
	
}
