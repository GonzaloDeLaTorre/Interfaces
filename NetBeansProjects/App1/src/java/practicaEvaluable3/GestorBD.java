/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaEvaluable3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author Gonzalo
 */
public class GestorBD {

    private static Connection con;

    static ArrayList<String> obtenerDatosArticulos() throws SQLException {
        java.sql.Connection conexion = con;
        String selec = "SELECT codigo, descripcion, stock, precioCompra, precioVenta FROM articulos";
        java.sql.Statement st = conexion.createStatement();
        ResultSet rs = st.executeQuery(selec);
        ArrayList<String> alArticulo = new ArrayList<String>();
        while (rs.next()) { 
            alArticulo.add(rs.getString(1) + "; " + rs.getString(2) + "; " + rs.getString(3) + "; " + rs.getString(4) + "; " + rs.getString(5));
        }
        rs.close();
        st.close();
       
        return alArticulo;
    }

    
    public GestorBD() {

    }

    
    public static java.sql.Connection conectarBBDD() throws Exception {
        try {
//			Class.forName("org.gjt.mm.mysql.Driver");
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost/interfaces", "root", "manager"); //manager en clase, en casa vacío
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al conectar con la BBDD", "Error conexion", JOptionPane.ERROR_MESSAGE);
        }
        return con;
    } //con.close(); cerrar al finalizar cualquier formulario clientes

}
