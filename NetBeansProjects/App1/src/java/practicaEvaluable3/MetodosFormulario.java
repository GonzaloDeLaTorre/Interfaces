/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaEvaluable3;

/**
 *
 * @author Gonzalo
 */
public class MetodosFormulario {
	
	public static char calcularLetraDni(String digitosDni){
        char letras[]={'T', 'R', 'W', 'A', 'G', 'M', 'Y',
                  'F', 'P', 'D', 'X', 'B', 'N', 'J', 'Z',
                  'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E'};
        return letras[Integer.valueOf(digitosDni)%23];
        
    }

}
