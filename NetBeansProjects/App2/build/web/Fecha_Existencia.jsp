<%-- 
    Document   : Fecha_Existencia
    Created on : 18-feb-2020, 17:20:52
    Author     : Gonzalo
--%>

<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de Pedidos</title>
    </head>
    <body>
        <h1>Gestión de extractos</h1>
        <%
            //CONECTANDO A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost/interfaces";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String password = ""; //manager en clase, vacío en casa
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, password);

            //Listar Datos de la Tabla Clientes:
            HttpSession sesion = request.getSession(); // Abro sesion
            Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la siguiente pág.
        %>

        <h2>Datos del cliente</h2>
        <%-- Creamos la Tabla: --%>
        <table style="text-align:left;" id="tablaDatos"> <%-- class="table table-bordered" --%>
            <thead> <%-- Cabecera donde van a ir los titulos: --%>
                <tr> <%-- fila: --%>
                    <th style="text-align:left;">Código</th> <%-- columna: --%>
                    <th style="text-align:left;">N.I.F.</th>
                    <th style="text-align:left;">Nombre</th>
                    <th style="text-align:left;">Apellidos</th>
                </tr>
            </thead>
            <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los clientes: --%> <%-- NO TIENE CIERRE --%>
                <tr>
                    <td><%= c.getCodigo() /*rs.getString("codigo")*/%></td>
                    <td><%= c.getNif()%></td>
                    <td><%= c.getNombre()%></td>
                    <td><%= c.getApellidos()%></td>
                </tr> 
            <thead>
                <tr>
                    <th style="text-align:left;">Domicilio</th>
                    <th style="text-align:left;">C.P.</th>
                    <th style="text-align:left;">Localidad</th>
                    <th style="text-align:left;">Total</th>
                </tr>
            </thead>
            <tr>
                <td><%= c.getDomicilio()%></td>
                <td><%= c.getCp()%></td>
                <td><%= c.getLocalidad()%></td>
                <td><%= c.getTotal_ventas()%></td>
            </tr>
        </table>

        <hr size="2px" color="grey"/> <%-- Línea separadora --%>
        
        <%  
            if(Metodos.buscarVenta(con) == true){ // Existe pedido   
        %>
        
        <%
            } else { // No existe pedido
        %>
        <h1>No ha hecho pedidos entre las fechas<br>fecha1 y fecha2</h1>
        
        <hr size="2px" color="grey"/> <%-- Línea separadora --%>
        
        <form action="Cliente_Existencia.jsp" method="post" id="formulario" onsubmit="return enviar()">
            <input type="submit" value="Nuevas fechas" id="nuevaFecha"> <!-- No hay que poner onClick puesto que lo recoge en su predefino que ponemos en el form -> onsubmit -->
        </form>
        <p>
        <a href="gestion_extractos.jsp">Nuevo cliente</a> | <a href="index.jsp">Página principal</a>
        <%
            }
        %>
    </body>
</html>
