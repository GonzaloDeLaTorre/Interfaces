<%-- 
    Document   : gestion_extractos
    Created on : 18-feb-2020, 17:11:43
    Author     : Gonzalo
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de extractos</title>
    </head>
    <body>
        <h1>Gestión de extractos</h1>
        <form action="Cliente_Existencia.jsp" method="post" id="formulario" onsubmit="return enviar()">
            Código de cliente <input type="text" name="txtCodigo" id="cajaCodigo" autofocus="" pattern="[A-z0-9]{0,6}" maxlength="6" size="6" title="Solo se admiten de 1 a 6 dígitos alfanuméricos."><p>

            <input type="submit" value="Aceptar" accesskey="A"> <!-- No hay que poner onClick puesto que lo recoge en su predefino que ponemos en el form -> onsubmit -->
            <input type="reset" value="Cancelar" onclick="reset2()" accesskey="C"><p>
        </form>

        <!--action="tercera.jsp?varForm=" method="post"-->

        <a href="index.jsp">Página principal</a>
    </body>
    <script> // Aquí tenemos las funciones que llamamos en los botones			}
        function enviar() {
            with (document.getElementById("cajaCodigo")) {
                if (document.getElementById("cajaCodigo").value === "") { // Si la caja de Código está vacía
                    alert("Debes introducir un dato.");
                    document.getElementById("cajaCodigo").focus();
                    return false;
                } else {
                    for (i = 1; value.length < 6; i++) {
                        value = "0" + value;
                    }
                }
            }
        }
        function reset2() {
//			document.getElementById("formulario").reset();
            document.getElementById("cajaCodigo").focus();
        }
    </script>
</html>
