/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package practicaEvaluable3;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Gonzalo
 */
public class Metodos {

    public static boolean buscarCliente(Connection con, String codigo) throws SQLException {
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM clientes WHERE codigo='" + codigo +"'");
        if (rs.next()) {
            return true;
        } else {
            return false;
        }
    }

    public static boolean buscarArticulo(Connection con, String codigo) throws SQLException {
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM articulos WHERE codigo='" + codigo +"'");
        if (rs.next()) {
            return true;
        } else {
            return false;
        }
    }

    public static Cliente añadirCliente(Connection con, String codigo) throws SQLException {
        Cliente c = null;
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select * from clientes where codigo='" + codigo +"'");
        while (rs.next()) {
            c = new Cliente(rs.getString(1), rs.getString(2), rs.getString(4), rs.getString(3), rs.getString(5), rs.getString(6), rs.getString(7), rs.getFloat(12));
        }
        rs.close();
        st.close();
        return c;
    }

    public static Articulos añadirArticulo(Connection con, String codigo) throws SQLException {
        Articulos a = null;
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("select * from articulos where codigo='" + codigo +"'");
        while (rs.next()) {
            a = new Articulos(rs.getString(1), rs.getString(2), rs.getFloat(3), rs.getFloat(6), rs.getFloat(5));
        }
        rs.close();
        st.close();
        return a;
    }

    public static void guardarPedido(Connection con, String cliente, String articulo, float unidades) throws SQLException {
        String insert = "INSERT INTO pedidosWeb (cliente,articulo,unidades,fecha) VALUES ('" + cliente + "', '" + articulo + "', " + unidades + ", CURRENT_DATE)";
        java.sql.PreparedStatement pst = con.prepareStatement(insert);
        pst.executeUpdate();
        pst.close();
    }
    
    public static void guardarTotalVenta(Connection con, String codigo, float totalVentas) throws SQLException {
        String update = "UPDATE clientes SET total_ventas=" + totalVentas + " where codigo='" + codigo +"'";
        java.sql.PreparedStatement pst = con.prepareStatement(update);
        pst.executeUpdate();
        pst.close();
    }

    public static boolean buscarVenta(Connection con) throws SQLException {
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM clientes WHERE total_ventas>0");
        if (rs.next()) {
            return true;
        } else {
            return false;
        }
    }
	
	public static ArrayList<PedidosWeb> buscarPedidos(Connection con, String fechaDesde, String fechaHasta, String cliente) throws SQLException {
        PedidosWeb p = null;
		ArrayList<PedidosWeb> al = new ArrayList<PedidosWeb>();
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery("SELECT * FROM pedidosWeb WHERE cast(fecha as date) >= '"+fechaDesde+"' AND cast(fecha as date) <= '"+fechaHasta+"' and cliente="+cliente+"");
        while (rs.next()) {
            p = new PedidosWeb(rs.getString(1), rs.getString(2), rs.getFloat(3), rs.getString(4));
			al.add(p);
        }
        rs.close();
        st.close();
        return al;
    }
    
}
