<%-- 
    Document   : Factura_Extracto.jsp
    Created on : 19-feb-2020, 17:24:23
    Author     : Gonzalo
--%>

<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="practicaEvaluable3.PedidosWeb"%>
<%@page import="java.util.ArrayList"%>
<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de extractos</title>
    </head>
    <body>
        <%
            //CONECTANDO A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost/interfaces";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String password = ""; //manager en clase, vacío en casa
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, password);
            
            //Listar Datos de la Tabla Clientes:
            HttpSession sesion = request.getSession(); // Abro sesion
            Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la anterior pág.
            String cliente = c.getCodigo();
            /* En función de como haya mandado la información de la ant. pág. lo recogeré de una de las dos formas: igualando el ArrayList ó llamando al método que recoge las fechas. */
            String fechaDesde = (String) sesion.getAttribute("fechaDesde"); // Recoger fechas de la ant. pág.
            String fechaHasta = (String) sesion.getAttribute("fechaHasta");
//            ArrayList<PedidosWeb> al = (ArrayList) sesion.getAttribute("al"); // Recoger ArrayList de la ant. pág.
            ArrayList<PedidosWeb> al = Metodos.buscarPedidos(con, fechaDesde, fechaHasta, cliente);
            
            java.util.Date d = new java.util.Date();
            java.sql.Date date2 = new java.sql.Date(d.getTime());
            SimpleDateFormat plantilla = new SimpleDateFormat("dd-MM-yyyy");
            String tiempo = plantilla.format(date2);
        %>
        Fecha:  <%= tiempo%>

        <hr size="2px" color="grey"/>

        <h1 style="text-align: center">Empresa Proyecto Web de clase, S.A.</h1>
        <h2 style="text-align: center">N.I.F.: 28938475-J</h2>
        <h3 style="text-align: center">C/ Isla de Sálvora, 451 - 28400 Collado Villalba - Madrid</h3>

        <hr size="1px" color="grey"/>

        <p>
            <b>Cliente:</b> <%= c.getCodigo()%> <b>N.I.F.:</b> <%= c.getNif()%><br>
            <b>D./Dña.</b> <%= c.getNombre()%> <%= c.getApellidos()%><br>
            <%= c.getDomicilio()%><br>
            <%= c.getCp()%> <%= c.getLocalidad()%>
        </p>

        <hr size="1px" color="grey"/>

        <h1 style="text-align: center">Extracto de pedidos desde <%= fechaDesde%> hasta <%= fechaHasta%></h1>
        <p>
        <div style="text-align: center">
            <table style="margin: 0 auto;"> <%-- class="table table-bordered" --%>
                <thead> <%-- Cabecera donde van a ir los titulos: --%>
                    <tr> <%-- fila: --%>
                        <th style="text-align:left;">Fecha</th>
                        <th>Artículo</th> <%-- columna: --%>
                        <th>Unidades</th>
                    </tr>
                </thead>
                <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los articulos: --%> <%-- NO TIENE CIERRE --%>
                    <%
                        for (int f = 0; f < al.size(); f++) {%>
                    <tr>
                        <td><%= al.get(f).getFecha()%>&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= al.get(f).getArticulo()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td style="text-align: right"><%= (int) (al.get(f).getUnidades())%></td>
                    </tr> 
                    <%
                        }
                    %>  
            </table>
        </div>
        <br>    
            
        <hr size="1px" color="grey"/>
    </body>
</html>
