<%-- 
    Document   : Fecha_Existencia
    Created on : 18-feb-2020, 17:20:52
    Author     : Gonzalo
--%>

<%@page import="practicaEvaluable3.PedidosWeb"%>
<%@page import="java.util.ArrayList"%>
<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de extractos</title>
    </head>
    <body>
        <h1>Gestión de extractos</h1>
        <%
            //CONECTANDO A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost/interfaces";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String password = ""; //manager en clase, vacío en casa
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, password);

            //Listar Datos de la Tabla Clientes:
            HttpSession sesion = request.getSession(); // Abro sesion
            Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la siguiente pág.
            String cliente = c.getCodigo();
        %>

        <h2>Datos del cliente</h2>
        <%-- Creamos la Tabla: --%>
        <table style="text-align:left;" id="tablaDatos"> <%-- class="table table-bordered" --%>
            <thead> <%-- Cabecera donde van a ir los titulos: --%>
                <tr> <%-- fila: --%>
                    <th style="text-align:left;">Código</th> <%-- columna: --%>
                    <th style="text-align:left;">N.I.F.</th>
                    <th style="text-align:left;">Nombre</th>
                    <th style="text-align:left;">Apellidos</th>
                </tr>
            </thead>
            <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los clientes: --%> <%-- NO TIENE CIERRE --%>
                <tr>
                    <td><%= c.getCodigo() /*rs.getString("codigo")*/%></td>
                    <td><%= c.getNif()%></td>
                    <td><%= c.getNombre()%></td>
                    <td><%= c.getApellidos()%></td>
                </tr> 
            <thead>
                <tr>
                    <th style="text-align:left;">Domicilio</th>
                    <th style="text-align:left;">C.P.</th>
                    <th style="text-align:left;">Localidad</th>
                    <th style="text-align:left;">Total</th>
                </tr>
            </thead>
            <tr>
                <td><%= c.getDomicilio()%></td>
                <td><%= c.getCp()%></td>
                <td><%= c.getLocalidad()%></td>
                <td style="text-align: center">0</td>
            </tr>
        </table>
        <br>            
        <hr size="1px" color="grey"/> <%-- Línea separadora --%>

        <%
            String fechaDesde = request.getParameter("Anio1") + "-" + request.getParameter("Mes1") + "-" + request.getParameter("Dia1"); // Recojo fechaDesde de la anterior pág.
            String fechaHasta = request.getParameter("Anio2") + "-" + request.getParameter("Mes2") + "-" + request.getParameter("Dia2"); // Recojo fechaHasta de la anterior pág.
            ArrayList<PedidosWeb> al = Metodos.buscarPedidos(con, fechaDesde, fechaHasta, cliente);
            /* Podemos mandar la información a la sig. pág. de dos formas: mandando el ArrayList ó mandando las fechas */
//            sesion.setAttribute("al", al); // Mandar ArrayList a la sig. pág.
            sesion.setAttribute("fechaDesde", fechaDesde); // MAndar fechas a la sig. pág.
            sesion.setAttribute("fechaHasta", fechaHasta);
            if (al.size() != 0) { // Existe pedido
        %>
        <h2>Extracto de pedidos</h2>
        <h3><b>Del <%= fechaDesde%> al <%= fechaHasta%></b></h3>
        <div style="text-align:left;">					
				
            <table> <%-- class="table table-bordered" --%>
                <thead> <%-- Cabecera donde van a ir los titulos: --%>
                    <tr> <%-- fila: --%>
                        <th style="text-align:left;">Fecha</th>
                        <th>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Artículo</th> <%-- columna: --%>
                        <th>Unidades</th>
                    </tr>
                </thead>
                <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los articulos: --%> <%-- NO TIENE CIERRE --%>
                    <%
                        for (int f = 0; f < al.size(); f++) {%>
                    <tr>
                        <td><%= al.get(f).getFecha()%>&nbsp;</td>
                        <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<%= al.get(f).getArticulo()%>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                        <td style="text-align: right"><%= (int) (al.get(f).getUnidades())%></td>
                    </tr> 
                    <%
                        }
                    %>  
            </table>
        </div>
        <br>
        <hr size="2px" color="grey"/> <%-- Línea separadora --%>

        <h2>Extracto finalizado</h2>
        Si desea imprimir el extracto se abrirá una nueva ventana con el extracto a imprimir.<br>
        En esta ventana, abra el menú Archivo y ejecute la poción Imprimir.<br>
        En la nueva ventana, seleccione su impresora y pulse Imprimir.<br>
        Después, cierre la ventana que contiene el extracto a Imprimir.<br>
        Para imprimir el extracto pulse <a href="Factura_Extracto.jsp" target="_blank">aquí</a>. Si no va a imprimir, puede regresar a la <a href="index.jsp">página principal</a>.



        <%
        } else { // No existe pedido
//              if(fechaDesde > fechaHasta){
//                  
//              }  else {
//                  
//              }
        %>
        <h1>No ha hecho pedidos entre las fechas<br><%= fechaDesde%> y <%= fechaHasta%></h1>

        <hr size="2px" color="grey"/> <%-- Línea separadora --%>

        <form action="Cliente_Existencia.jsp" method="post" id="formulario" onsubmit="return enviar()">
            <input type="submit" value="Nuevas fechas" id="nuevaFecha"> <!-- No hay que poner onClick puesto que lo recoge en su predefino que ponemos en el form -> onsubmit -->
        </form>
        <p>
            <a href="gestion_extractos.jsp">Nuevo cliente</a> | <a href="index.jsp">Página principal</a>
            <%
                }
            %>
    </body>
</html>
