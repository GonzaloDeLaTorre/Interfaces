
<%-- 
    Document   : cuarta
    Created on : 06-feb-2020, 11:25:31
    Author     : alumno
--%>
<%@page import="practicaEvaluable3.Cliente"%>
<%@page import="practicaEvaluable3.Metodos"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.PreparedStatement"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Connection"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Gestión de extractos</title>
    </head>
    <body>
        <h1>Gestión de extractos</h1>
        <%
            //CONECTANDO A LA BASE DE DATOS:
            Connection con;
            String url = "jdbc:mysql://localhost/interfaces";
            String Driver = "com.mysql.jdbc.Driver";
            String user = "root";
            String password = ""; //manager en clase, vacío en casa
            Class.forName(Driver);
            con = DriverManager.getConnection(url, user, password);

            //Listar Datos de la Tabla Clientes:
            HttpSession sesion = request.getSession(); // Abro sesion
            Cliente c = (Cliente) sesion.getAttribute("Cliente"); // Recojo cliente de la siguiente pág.
            String cod_Cliente = request.getParameter("txtCodigo"); // Recojo código del cliente enviado de la anterior pág.
            if (cod_Cliente == null) {
                cod_Cliente = c.getCodigo();
            }
            if (Metodos.buscarCliente(con, cod_Cliente) == true) { // Compruebo si existe el cliente, en el caso que SÍ entra en el if.
                c = Metodos.añadirCliente(con, cod_Cliente); // Creo cliente y le Añado valores
                sesion.setAttribute("Cliente", c); // Guardo el cliente creado en la sesion para utilizarlo después.
        %>

        <h2>Datos del cliente</h2>
        <%-- Creamos la Tabla: --%>
        <table style="text-align:left;" id="tablaDatos"> <%-- class="table table-bordered" --%>
            <thead> <%-- Cabecera donde van a ir los titulos: --%>
                <tr> <%-- fila: --%>
                    <th style="text-align:left;">Código</th> <%-- columna: --%>
                    <th style="text-align:left;">N.I.F.</th>
                    <th style="text-align:left;">Nombre</th>
                    <th style="text-align:left;">Apellidos</th>
                </tr>
            </thead>
            <tbody id="tbodys"> <%-- Cuerpo donde van a ir los datos de los clientes: --%> <%-- NO TIENE CIERRE --%>
                <tr>
                    <td><%= c.getCodigo() /*rs.getString("codigo")*/%></td>
                    <td><%= c.getNif()%></td>
                    <td><%= c.getNombre()%></td>
                    <td><%= c.getApellidos()%></td>
                </tr> 
            <thead>
                <tr>
                    <th style="text-align:left;">Domicilio</th>
                    <th style="text-align:left;">C.P.</th>
                    <th style="text-align:left;">Localidad</th>
                    <th style="text-align:left;">Total</th>
                </tr>
            </thead>
            <tr>
                <td><%= c.getDomicilio()%></td>
                <td><%= c.getCp()%></td>
                <td><%= c.getLocalidad()%></td>
                <td>0</td>
            </tr>
        </table>

        <hr size="2px" color="grey"/> <%-- Línea separadora --%>

        <h2>Fechas del extracto</h2>
        <form action="Fecha_Existencia.jsp" method="get" name="formExtractos" onsubmit="return enviar()">
            <table>
                <tr>
                    <td>Desde</td>
                    <td>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td>Hasta</td>
                </tr>
            </table>
            <table>
                <tr>
                    <td><b>A&ntilde;o</b></td>
                    <td><b>Mes</b></td>
                    <td><b>D&iacute;a</b></td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td><b>A&ntilde;o</b></td>
                    <td><b>Mes</b></td>
                    <td><b>D&iacute;a</b></td>
                </tr>
                <tr>
                    <td>
                        <select id="Anio1" name="Anio1" onchange="ActualizarFecha1()">
                            <%
                                int Anio = (Integer) sesion.getAttribute("anioActual");
                                int HastaAnio = Anio - 5;
                                while (Anio > HastaAnio) {
                            %>
                            <option value="<%= Anio%>"><%= Anio%>
                                <%
                                        Anio = Anio - 1;
                                    }
                                %>
                        </select>
                    </td>
                    <td>
                        <select id="Mes1" name="Mes1" onchange="ActualizarFecha1()">
                            <%
                                for (int i = 1; i <= 12; i++) {
                            %>
                            <option value="<%= i%>"><%= i%>
                                <%
                                    }
                                %>
                        </select>
                    </td>
                    <td>
                        <select id="Dia1" name="Dia1" onchange="ActualizarFecha1()">
                            <%
                                for (int i = 1; i <= 31; i++) {%>
                            <option value="<%= i%>"><%= i%>
                                <% } %>
                        </select>
                    </td>
                    <td>&nbsp;&nbsp;&nbsp;</td>
                    <td>
                        <select id="Anio2" name="Anio2" onchange="ActualizarFecha2()">
                            <%
                                Anio = (Integer) sesion.getAttribute("anioActual");
                                HastaAnio = Anio - 5;
                                while (Anio > HastaAnio) {%>
                            <option value="<%= Anio%>"><%= Anio%>
                                <% Anio = Anio - 1;
                                    } %>
                        </select>
                    </td>
                    <td>
                        <select id="Mes2" name="Mes2" onchange="ActualizarFecha2()">
                            <%
                                for (int i = 1; i <= 12; i++) {%>
                            <option value="<%= i%>"><%= i%>
                                <% } %>
                        </select>
                    </td>
                    <td>
                        <select id="Dia2" name="Dia2" onchange="ActualizarFecha2()">
                            <%
                                for (int i = 1; i <= 31; i++) {%>
                            <option value="<%= i%>"><%= i%>
                                <% } %>
                        </select>
                    </td>
                </tr>
            </table>
            <br><br>
            <input type="submit" name="cmdAceptar" value="Aceptar">
            <br><br>
        </form>

        <hr size="2px" color="grey"/>

        <a href="gestion_extractos.jsp">Nuevo cliente</a> | <a href="index.jsp">Página principal</a>


        <%
        } else { // Cliente no existe
        %>

        <h2>El cliente con código <%= request.getParameter("txtCodigo")%> no existe</h2><p>

            <a href="gestion_extractos.jsp">Nuevo cliente</a> | <a href="index.jsp">Página principal</a>
            <%
                }
            %>
    </body>
    <script> // Aquí tenemos las funciones que llamamos en los botones			}
        function enviar() {
            fechaDesde = document.getElementById("Anio1").value + document.getElementById("Mes1").value + document.getElementById("Dia1").value;
            fechaHasta = document.getElementById("Anio2").value + document.getElementById("Mes2").value + document.getElementById("Dia2").value;

            if (fechaDesde > fechaHasta) {
                alert("La fecha 'Desde' no puede ser mayor que la fecha 'Hasta'.");
                return false;
            }
        }
        function ActualizarFecha1() {
            dias = devuelveDias(document.formExtractos.Mes1.value,
                    document.formExtractos.Anio1.value);

            var combo = document.getElementById("Anio1");
            var selected = combo.options[combo.selectedIndex].text;

            if (selected === "2020") {
                if (document.getElementById("Mes1").value > 2) {
                    alert("Mes incorrecto");
                }
                if (document.getElementById("Mes1").value === "1") {
                    document.formExtractos.Mes1.value = 1;
                } else {
                    document.formExtractos.Mes1.value = 2;
                }
                if (document.getElementById("Mes1").value === "2") {  // MAAAAL
                    if (document.getElementById("Dia1").value > "26") {
                        alert("Dia incorrecto");
                        document.formExtractos.Dia1.value = 26;
                    }
                }
            }

            with (document.formExtractos.Dia1) {
                i = length;
                if (dias < length) {
                    61;
                    while (i >= dias) {
                        options[i] = null;
                        i = i - 1;
                    }
                } else {
                    while (i < dias) {
                        /* En new Option tengo que utilizar i+1 porque si
                         pongo i y, en la instrucción antes del bucle
                         donde toma valor i, utilizo
                         document.formExtractos.Dia1.length+1, me deja
                         una opción en blanco entre el día 29 y 30.
                         No sé por qué. */
                        options[i] = new Option(i + 1, i + 1);
                        i = i + 1;
                    }
                }
            }
        }
        function ActualizarFecha2() {
            dias = devuelveDias(document.formExtractos.Mes2.value,
                    document.formExtractos.Anio2.value);
                    
            var combo = document.getElementById("Anio2");
            var selected = combo.options[combo.selectedIndex].text;

            if (selected === "2020") {
                if (document.getElementById("Mes2").value > 2) {
                    alert("Mes incorrecto");
                }
                if (document.getElementById("Mes2").value === "1") {
                    document.formExtractos.Mes2.value = 1;
                } else {
                    document.formExtractos.Mes2.value = 2;
                }
                if (document.getElementById("Mes2").value === "2") {  // MAAAAL
                    if (document.getElementById("Dia2").value > "26") {
                        alert("Dia incorrecto");
                        document.formExtractos.Dia2.value = 26;
                    }
                }
            }
            
            with (document.formExtractos.Dia2) {
                i = length;
                if (dias < length) {
                    while (i >= dias) {
                        options[i] = null;
                        i = i - 1;
                    }
                } else {
                    while (i < dias) {
                        /* En new Option tengo que utilizar i+1 porque si
                         pongo i y, en la instrucción antes del bucle
                         donde toma valor i, utilizo
                         document.formExtractos.Dia1.length+1, me deja
                         una opción en blanco entre el día 29 y 30.
                         No sé por qué. */
                        options[i] = new Option(i + 1, i + 1);
                        62
                        i = i + 1
                    }
                }
            }
        }
        function devuelveDias(mes, anio) {
            dias = 0;
            switch (mes) {
                case "1":
                    ;
                case "3":
                    ;
                case "5":
                    ;
                case "7":
                    ;
                case "8":
                    ;
                case "10":
                    ;
                case "12":
                    dias = 31;
                    break
                case "4":
                    ;
                case "6":
                    ;
                case "9":
                    ;
                case "11":
                    dias = 30;
                    break
                case "2":
                    if (anio % 4 === 0) {
                        dias = 29;
                    } else {
                        dias = 28;
                    }
                    break
            }
            return (dias);
        }
    </script>
</html>
