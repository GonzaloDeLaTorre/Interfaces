create database if not exists interfaces;
use interfaces;

 -- Tabla Clientes
 CREATE TABLE clientes(
 codigo VARCHAR(6) PRIMARY KEY,
 nif VARCHAR(9),
 apellidos VARCHAR(35),
 nombre VARCHAR(15),
 domicilio VARCHAR(40),
 cp VARCHAR(5),
 localidad VARCHAR(20),
 telefono VARCHAR(9),
 movil VARCHAR(9),
 fax VARCHAR(9),
 email VARCHAR(20),
 total_ventas FLOAT
 );

-- Tabla Provedores
CREATE TABLE proveedores(
 codigo VARCHAR(6) PRIMARY KEY,
 nif VARCHAR(9),
 apellidos VARCHAR(35),
 nombre VARCHAR(15),
 domicilio VARCHAR(40),
 cp VARCHAR(5),
 localidad VARCHAR(20),
 telefono VARCHAR(9),
 movil VARCHAR(9),
 fax VARCHAR(9),
 email VARCHAR(20),
 total_compras FLOAT
 );
 
 -- Tabla Articulo
 CREATE TABLE articulos(
 codigo VARCHAR(6) PRIMARY KEY,
 descripcion VARCHAR(25),
 stock FLOAT,
 stockMinimo FLOAT,
 precioCompra FLOAT,
 precioVenta FLOAT
 );
 
 -- Tabla Historica
 CREATE TABLE historica (
	cliente varchar(6) NULL,
	proveedor varchar(6) NULL,
	articulo varchar(6) NULL,
	unidades FLOAT NULL,
	fecha DATE NULL,
	CONSTRAINT historica_clientes_fk FOREIGN KEY (cliente) REFERENCES interfaces.clientes(codigo),
	CONSTRAINT historica_articulos_fk FOREIGN KEY (articulo) REFERENCES interfaces.articulos(codigo),
	CONSTRAINT historica_proveedores_fk FOREIGN KEY (proveedor) REFERENCES interfaces.proveedores(codigo)
);

-- insert into historica (cliente,proveedor,articulo,unidades,fecha) values('000001','000001','000001',2,'1000-01-01');
